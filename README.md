# Case-based reasoning for Drupal

Case-based reasoning is an AI paradigm that solves new problems by reusing stored cases. 
This module adds fields that allow you to create your own knowledge base. 
For more information about CBR, check out this wikipedia page: https://en.wikipedia.org/wiki/Case-based_reasoning

For content types that have been configured accordingly, a list of the most similar nodes is displayed for each node. A suitable content can be selected from this list and a new solution can be generated from it.

However, the module can also be used in applications where the aim is to compare content across its structure and to find similar content to existing content. The output of the most similar content can be configured via views. This can be used for showing similar blogposts and news articles or showing product recommendation.

## Getting started
Go to Structure -> Content types and create a new content type. Add at least one field from the cbr module. If more than one node of this type is created, the similarity between the new new node and all nodes stored in system, is calculated.