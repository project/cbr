<?

namespace Drupal\cbr\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\Entity\Node;

/**
 * Checks access for displaying the similar cases tab.
 */
class SimilarCasesAccessCheck implements AccessInterface
{
    protected RouteMatchInterface $routeMatch;
    protected EntityTypeManagerInterface $entityTypeManager;
    protected LoggerChannelInterface $logger;

    /**
     *  Constructor, objects are injected by the service container.
     * 
     * @param Drupal\Core\Routing\RouteMatchInterface $routeMatch 
     * The route match object.
     * 
     * @param Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
     * The entity type manager.
     * 
     * @param Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory 
     * The logger channel factory.
     */
    public function __construct(
        RouteMatchInterface $routeMatch,
        EntityTypeManagerInterface $entityTypeManager,
        LoggerChannelFactoryInterface $loggerFactory
    ) {
        $this->routeMatch = $routeMatch;
        $this->entityTypeManager = $entityTypeManager;
        $this->logger = $loggerFactory->get('cbr');
    }

    /**
     * Check access for displaying the similar cases tab.
     *
     * @param \Drupal\Core\Session\AccountInterface $account
     * Run access checks for this account.
     *
     * @return \Drupal\Core\Access\AccessResultInterface
     * The access result.
     */
    public function access(AccountInterface $account)
    {
        $parameters = $this->routeMatch->getParameters();
        $node = $parameters->get('node');

        if (is_numeric($node)) {
            $node = $this->entityTypeManager->getStorage('node')->load($node);
        }

        if ($node instanceof Node && cbr_is_enabled_on_entity($node)) {
            $accessResult = AccessResult::allowedIfHasPermission($account, 'cbr access similar cases');  // Check if the user has the permission to access the Similar Cases tab. 
            if (!$accessResult->isAllowed()) {
                $this->logger->info('Access denied to Similar Cases tab for user %user.', ['%user' => $account->getAccountName()]);
            }
            return $accessResult;
        }

        return AccessResult::forbidden();
    }
}