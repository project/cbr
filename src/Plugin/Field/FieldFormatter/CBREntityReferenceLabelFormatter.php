<?php

namespace Drupal\cbr\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;

/**
 * Plugin implementation of the 'cbr entity reference label' formatter.
 *
 * @FieldFormatter(
 *   id = "cbr_entity_reference_label",
 *   label = @Translation("Label"),
 *   description = @Translation("Display the label of the referenced entities."),
 *   field_types = {
 *     "cbr_node_reference",
 *     "cbr_solution_reference",
 *     "cbr_taxonomy_reference",
 *   }
 * )
 */
class CBREntityReferenceLabelFormatter extends EntityReferenceLabelFormatter
{
}