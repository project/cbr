<?php

namespace Drupal\cbr\Plugin\Field\FieldFormatter;

use Drupal\datetime\Plugin\Field\FieldFormatter\DateTimeDefaultFormatter;

/**
 * Plugin implementation of the 'Default' formatter for 'cbr_datetime' fields.
 *
 * @FieldFormatter(
 *   id = "cbr_datetime_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "cbr_datetime"
 *   }
 * )
 */
class CBRDateTimeDefaultFormatter extends DateTimeDefaultFormatter
{
}