<?php

namespace Drupal\cbr\Plugin\Field\FieldFormatter;

use Drupal\text\Plugin\Field\FieldFormatter\TextDefaultFormatter;

/**
 * Plugin implementation of the 'text_default' formatter.
 *
 * @FieldFormatter(
 *   id = "cbr_text_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "cbr_text",
 *     "cbr_text_long",
 *   }
 * )
 */
class CBRTextDefaultFormatter extends TextDefaultFormatter
{
}