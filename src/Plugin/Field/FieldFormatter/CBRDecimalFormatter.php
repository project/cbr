<?php

namespace Drupal\cbr\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\DecimalFormatter;

/**
 * Plugin implementation of the 'number_decimal' formatter.
 *
 * The 'Default' formatter is different for integer fields on the one hand, and
 * for decimal and float fields on the other hand, in order to be able to use
 * different settings.
 *
 * @FieldFormatter(
 *   id = "cbr_number_decimal",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "cbr_decimal",
 *   }
 * )
 */
class CBRDecimalFormatter extends DecimalFormatter
{
}