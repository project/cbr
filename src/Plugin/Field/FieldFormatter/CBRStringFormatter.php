<?php

namespace Drupal\cbr\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\BasicStringFormatter;

/**
 * Plugin implementation of the 'basic_string' formatter.
 *
 * @FieldFormatter(
 *   id = "cbr_basic_string",
 *   label = @Translation("Plain text"),
 *   field_types = {
 *     "cbr_string_long", 
 *     "cbr_string" 
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class CBRStringFormatter extends BasicStringFormatter
{
}