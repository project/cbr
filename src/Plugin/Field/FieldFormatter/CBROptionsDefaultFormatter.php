<?php

namespace Drupal\cbr\Plugin\Field\FieldFormatter;

use Drupal\options\Plugin\Field\FieldFormatter\OptionsDefaultFormatter;

/**
 * Plugin implementation of the 'cbr_list_default' formatter.
 *
 * @FieldFormatter(
 *   id = "cbr_list_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "cbr_case_status",
 *   }
 * )
 */
class CBROptionsDefaultFormatter extends OptionsDefaultFormatter
{
}