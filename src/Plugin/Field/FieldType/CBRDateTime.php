<?

namespace Drupal\cbr\Plugin\Field\FieldType;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\field\Entity\FieldConfig;

/**
 * Plugin implementation of the 'datetime' field type.
 *
 * @FieldType(
 *   id = "cbr_datetime",
 *   label = @Translation("CBR DateTime"),
 *   description = @Translation("Create and store date values."),
 *   category = @Translation("Case Based Reasoning"),
 *   default_widget = "cbr_datetime_default",
 *   default_formatter = "cbr_datetime_default",
 *   list_class = "\Drupal\datetime\Plugin\Field\FieldType\DateTimeFieldItemList",
 *   constraints = {"DateTimeFormat" = {}},
 *   cardinality = 1
 * )
 */
class CBRDateTime extends DateTimeItem implements CBRFieldInterface
{
    /**
     * {@inheritdoc}
     */
    public function fieldSettingsForm(array $form, FormStateInterface $form_state): array
    {
        $form = CBRFieldHelper::cbrFieldSettingsForm($form, $form_state) + parent::fieldSettingsForm($form, $form_state);

        /** @var FieldConfig $field_config */
        $field_config = $form_state->getFormObject()->getEntity();
        $form['cbr_settings']['date_granularity'] = [
            '#type' => 'select',
            '#title' => t('Date granularity'),
            '#description' => t('Select the granularity of the date. This will affect the similarity. <br>If the field contains already data, a manual recalculation will be required.'),
            '#options' => [
                'day' => t('Day'),
                'month' => t('Month'),
                'year' => t('Year'),
            ],
            '#default_value' => $field_config->getThirdPartySetting('cbr', 'date_granularity', 'month'),
            '#required' => true
        ];
        $form['#entity_builders'][] = [$this, 'saveCBRFieldDateSettings'];
        return $form;
    }

    /**
     * Save the selected date granularity to the field config.
     * @param $entity_type The entity type.
     * @param FieldConfig $field_config The field config.
     * @param $form The form array.
     * @param FormStateInterface $form_state The form state.
     */
    public function saveCBRFieldDateSettings($entity_type, FieldConfig $fieldConfig, &$form, FormStateInterface $form_state)
    {
        $granularity = $form_state->getValue(['settings', 'cbr_settings', 'date_granularity']);
        $fieldConfig->setThirdPartySetting('cbr', 'date_granularity', $granularity);
    }

    /**
     * {@inheritdoc}
     */
    public function calculateSimilarity($value1, $value2, FieldConfig $field_config): float
    {
        return CBRFieldHelper::calculateSimilarityBetweenNumericValues($value1, $value2, null, null);
    }

    /**
     * {@inheritdoc}
     */
    public function summerize(array $values): float
    {
        return CBRFieldHelper::summerizeNumericValues($values);
    }

    /**
     * {@inheritdoc}
     */
    public function getValueForSimilarityCalculation(FieldConfig $field_config): float
    {
        $granularity = $field_config->getThirdPartySetting('cbr', 'date_granularity', 'month');
        return $this->date_granularity($this->date, $granularity);
    }

    private function date_granularity(DrupalDateTime $date, string $granularity): float
    {
        switch ($granularity) {
            case 'day':
                return $date->getTimestamp() / (60 * 60 * 24);
            case 'month':
                return $date->getTimestamp() / (60 * 60 * 24 * 30);
            case 'year':
                return $date->getTimestamp() / (60 * 60 * 24 * 365);
            default:
                return $date->getTimestamp();
        }
    }
}