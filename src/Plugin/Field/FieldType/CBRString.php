<?php

namespace Drupal\cbr\Plugin\Field\FieldType;

use Drupal\cbr\Plugin\Field\FieldType\CBRFieldHelper;
use Drupal\cbr\Plugin\Field\FieldType\CBRFieldInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;

/**
 * Defines the 'cbr_string' field type.
 *
 * @FieldType(
 *   id = "cbr_string",
 *   label = @Translation("CBR Text (plain)"),
 *   description = @Translation("A field containing a long string value."),
 *   category = @Translation("Case-based Reasoning"),
 *   default_widget = "cbr_string_textfield",
 *   default_formatter = "cbr_basic_string",
 *   cardinality = 1
 * )
 */
class CBRString extends StringItem implements CBRFieldInterface
{

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array
  {
    $form = CBRFieldHelper::cbrFieldSettingsForm($form, $form_state) + parent::fieldSettingsForm($form, $form_state);
    return CBRFieldHelper::stringFieldSettingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateSimilarity($value1, $value2, FieldConfig $field_config): float
  {
    $similarity_function = $field_config->getThirdPartySetting('cbr', 'similarity_function', 'cosine');
    return CBRFieldHelper::calculateSimilarityBetweenStringValues($value1, $value2, $similarity_function);
  }

  /**
   * {@inheritdoc}
   */
  public function summerize(array $values): string
  {
    return CBRFieldHelper::summarizeStringValues($values);
  }

  /**
   * {@inheritdoc}
   */
  public function getValueForSimilarityCalculation(FieldConfig $field_config): string
  {
    return $this->value;
  }
}
