<?php

namespace Drupal\cbr\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\DecimalItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;

/**
 * Defines the 'cbr_decimal' field type.
 *
 * @FieldType(
 *   id = "cbr_decimal",
 *   label = @Translation("CBR Number (decimal)"),
 *   description = @Translation("This field stores a number in the database in a fixed decimal format."),
 *   category = @Translation("Case Based Reasoning"),
 *   default_widget = "cbr_number",
 *   default_formatter = "cbr_number_decimal",
 *   cardinality = 1
 * )
 */
class CBRDecimalField extends DecimalItem implements CBRFieldInterface
{
    /**
     * {@inheritdoc}
     */
    public function fieldSettingsForm(array $form, FormStateInterface $form_state): array
    {
        return parent::fieldSettingsForm($form, $form_state) + CBRFieldHelper::cbrFieldSettingsForm($form, $form_state);
    }

    public function calculateSimilarity($value1, $value2, FieldConfig $field_config): float
    {
        return  CBRFieldHelper::calculateSimilarityBetweenNumericValues($value1, $value2, (float)$this->getSetting('min'), (float)$this->getSetting('max'));
    }

    public function summerize(array $fields): float
    {
        return CBRFieldHelper::summerizeNumericValues($fields);
    }

    /**
     * {@inheritdoc}
     */
    public function getValueForSimilarityCalculation(FieldConfig $field_config): float
    {
        return $this->value;
    }
}