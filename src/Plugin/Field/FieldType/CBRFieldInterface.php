<?php

namespace Drupal\cbr\Plugin\Field\FieldType;

use Drupal\field\Entity\FieldConfig;

/**
 * Interface for defining case base reasoning field types.
 * Every field type that implements this interface will be able to be used in case base reasoning.
 */
interface CBRFieldInterface
{
  /**
   * Calculates the similarity between two values.
   * @param $value1 The first value.
   * @param $value2 The second value.
   * @param FieldConfig $field_config The field config.
   * @return float The similarity [0-1] between the two values.
   */
  function calculateSimilarity(mixed $value1, mixed $value2, FieldConfig $field_config): float;

  /**
   * Get the value of the field prepared for use in the similarity calculation.
   * @param FieldConfig $field_config The field config.
   * @return mixed The value of the field, prepared for use in the similarity calculation.
   */
  function getValueForSimilarityCalculation(FieldConfig $field_config): mixed;

  /**
   * Summerize multiple values into a single value. 
   * @param array $values The values to summerize.
   * @return mixed The summerized value.
   */
  function summerize(array $values): mixed;
}