<?php

namespace Drupal\cbr\Plugin\Field\FieldType;


use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\text\Plugin\Field\FieldType\TextLongItem;

/**
 * Plugin implementation of the 'field_cbr_boolean' field type.
 *
 * @FieldType(
 *   id = "cbr_text_long",
 *   label = @Translation("CBR Text (formatted, long)"),
 *   module = "cbr",
 *   category = @Translation("Case Based Reasoning"),
 *   description = @Translation("This field stores a text with a text format."),
 *   default_widget = "cbr_text_textarea",
 *   default_formatter = "cbr_text_default",
 *   cardinality = 1
 * )
 */
class CBRTextLong extends TextLongItem implements CBRFieldInterface
{
  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array
  {
    $form = CBRFieldHelper::cbrFieldSettingsForm($form, $form_state) + parent::fieldSettingsForm($form, $form_state);
    return CBRFieldHelper::stringFieldSettingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateSimilarity($value1, $value2, FieldConfig $field_config): float
  {
    $similarity_function = $field_config->getThirdPartySetting('cbr', 'similarity_function', 'cosine');
    return CBRFieldHelper::calculateSimilarityBetweenStringValues($value1, $value2, $similarity_function);
  }

  /**
   * {@inheritdoc}
   */
  public function summerize(array $values): string
  {
    return CBRFieldHelper::summarizeStringValues($values);
  }

  /**
   * {@inheritdoc}
   */
  public function getValueForSimilarityCalculation(FieldConfig $field_config): string
  {
    return $this->value;
  }
}