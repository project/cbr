<?php

namespace Drupal\cbr\Plugin\Field\FieldType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\options\Plugin\Field\FieldType\ListIntegerItem;

/**
 * Defines the 'cbr_case_status' field type.
 *
 * @FieldType(
 *   id = "cbr_case_status",
 *   label = @Translation("CBR Case Status"),
 *   description = @Translation("This field stores the case status in the database."),
 *   category = @Translation("Case-based Reasoning - Case Metadata"),
 *   default_widget = "options_select",
 *   default_formatter = "cbr_list_default",
 *   cardinality = 1
 * )
 */
class CBRCaseStatus extends ListIntegerItem
{
    const CASE_STATUS_NEW = 0;
    const CASE_STATUS_TESTING_SOLUTION = 1;
    const CASE_STATUS_SOLUTION_ACCEPTED = 2;
    const CASE_STATUS_SOLUTION_REJECTED = 3;


    /**
     * {@inheritdoc}
     */
    public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data)
    {
        $allowed_values = $this->allowedValues();
        $allowed_values_function = $this->getSetting('allowed_values_function');

        $element['allowed_values'] = [
            '#type' => 'textarea',
            '#title' => t('Allowed values list'),
            '#default_value' => $this->allowedValuesString($allowed_values),
            '#rows' => 4,
            '#access' => empty($allowed_values_function),
            '#element_validate' => [[static::class, 'validateAllowedValues']],
            '#field_has_data' => $has_data,
            '#field_name' => $this->getFieldDefinition()->getName(),
            '#entity_type' => $this->getEntity()->getEntityTypeId(),
            '#allowed_values' => $allowed_values,
            '#attributes' => ['disabled' => 'disabled']
        ];

        return $element;
    }

    private function allowedValues()
    {
        return [
            self::CASE_STATUS_NEW => t('New'),
            self::CASE_STATUS_TESTING_SOLUTION => t('Testing solution'),
            self::CASE_STATUS_SOLUTION_ACCEPTED => t('Solution accepted'),
            self::CASE_STATUS_SOLUTION_REJECTED => t('Solution rejected'),
        ];
    }
}