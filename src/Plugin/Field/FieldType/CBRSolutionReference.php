<?php

namespace Drupal\cbr\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'entity_reference' entity field type.
 *
 * Supported settings (below the definition's 'settings' key) are:
 * - target_type: The entity type to reference. Required.
 *
 * @FieldType(
 *   id = "cbr_solution_reference",
 *   label = @Translation("CBR Reference to solution"),
 *   description = @Translation("An entity field containing an entity reference."),
 *   category = @Translation("Case-based Reasoning - Case Metadata"),
 *   default_widget = "cbr_entity_reference_autocomplete",
 *   default_formatter = "cbr_entity_reference_label",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 *   cardinality = -1,
 * )
 */
class CBRSolutionReference extends EntityReferenceItem
{
    /**
     * {@inheritdoc}
     */
    public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data)
    {
        $element['target_type'] = [
            '#type' => 'select',
            '#title' => t('Type of item to reference'),
            '#default_value' => 'node',
            '#options' => [
                'node' => t('Node'),
            ],
            '#required' => TRUE,
            '#disabled' => TRUE,
            '#size' => 1,
        ];
        return $element;
    }

    /**
     * {@inheritdoc}
     */
    public static function getPreconfiguredOptions()
    {
        $options = [];
        return $options;
    }
}
