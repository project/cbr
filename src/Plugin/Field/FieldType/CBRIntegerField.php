<?php

namespace Drupal\cbr\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\IntegerItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;

/**
 * Defines the 'integer' field type.
 *
 * @FieldType(
 *   id = "cbr_integer",
 *   label = @Translation("CBR Number (integer)"),
 *   description = @Translation("This field stores a number in the database as an integer. This field can be used for case based reasoning."), 
 *   category = @Translation("Case-based Reasoning"),
 *   default_widget = "cbr_number",
 *   default_formatter = "cbr_number_integer",
 *   cardinality = 1
 * )
 */
class CBRIntegerField extends IntegerItem implements CBRFieldInterface
{

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array
  {
    return CBRFieldHelper::cbrFieldSettingsForm($form, $form_state) + parent::fieldSettingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateSimilarity($value1, $value2, FieldConfig $fieldConfig): float
  {
    return CBRFieldHelper::calculateSimilarityBetweenNumericValues($value1, $value2, (float)$fieldConfig->getSetting('min'), (float)$fieldConfig->getSetting('max'));
  }

  /**
   * {@inheritdoc}
   */
  public function getValueForSimilarityCalculation(FieldConfig $field_config): int
  {
    return $this->value;
  }


  /**
   * {@inheritdoc}
   */
  public function summerize(array $fields): float
  {
    return CBRFieldHelper::summerizeNumericValues($fields);
  }
}
