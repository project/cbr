<?php

namespace Drupal\cbr\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;

/**
 * Plugin implementation of the 'cbr options_select' widget.
 *
 * @FieldWidget(
 *   id = "cbr_options_select",
 *   label = @Translation("Select list"),
 *   field_types = {
 *     "cbr_case_status",
 *   },
 *   multiple_values = TRUE
 * )
 */
class CBROptionsSelectWidget extends OptionsSelectWidget
{
}