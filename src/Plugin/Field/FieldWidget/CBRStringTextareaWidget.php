<?php

namespace Drupal\cbr\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextareaWidget;

/**
 * Plugin implementation of the 'cbr_string_textarea' widget.
 *
 * @FieldWidget(
 *   id = "cbr_string_textarea",
 *   label = @Translation("Text area (multiple rows)"),
 *   field_types = {
 *     "cbr_string",
 *     "cbr_string_long",
 *   }
 * )
 */
class CBRStringTextareaWidget extends StringTextareaWidget
{
}