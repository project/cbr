<?php

namespace Drupal\cbr\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;

/**
 * Plugin implementation of the 'cbr_entity_reference_autocomplete' widget.
 *
 * @FieldWidget(
 *   id = "cbr_entity_reference_autocomplete",
 *   label = @Translation("Autocomplete"),
 *   description = @Translation("An autocomplete text field."),
 *   field_types = {
 *     "cbr_node_reference",
 *     "cbr_taxonomy_reference",
 *     "cbr_solution_reference",
 *   }
 * )
 */
class CBREntityReferenceAutocompleteWidget extends EntityReferenceAutocompleteWidget
{
}