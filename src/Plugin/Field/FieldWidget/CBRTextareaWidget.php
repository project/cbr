<?php

namespace Drupal\cbr\Plugin\Field\FieldWidget;

use Drupal\text\Plugin\Field\FieldWidget\TextareaWidget;


/**
 * Plugin implementation of the 'cbr_text_textarea' widget.
 *
 * @FieldWidget(
 *   id = "cbr_text_textarea",
 *   label = @Translation("Text area (multiple rows)"),
 *   field_types = {
 *     "cbr_text_long"
 *   }
 * )
 */
class CBRTextareaWidget extends TextareaWidget
{
}