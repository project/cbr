<?php

namespace Drupal\cbr\Plugin\Field\FieldWidget;

use Drupal\text\Plugin\Field\FieldWidget\TextfieldWidget;

/**
 * Plugin implementation of the 'cbr_text_textfield' widget.
 *
 * @FieldWidget(
 *   id = "cbr_text_textfield",
 *   label = @Translation("Text field"),
 *   field_types = {
 *     "cbr_text",
 *     "cbr_text_long",
 *   },
 * )
 */
class CBRTextfieldWidget extends TextfieldWidget
{
}