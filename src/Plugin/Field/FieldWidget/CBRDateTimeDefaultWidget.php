<?php

namespace Drupal\cbr\Plugin\Field\FieldWidget;

use Drupal\datetime\Plugin\Field\FieldWidget\DateTimeDefaultWidget;

/**
 * Plugin implementation of the 'datetime_default' widget.
 *
 * @FieldWidget(
 *   id = "cbr_datetime_default",
 *   label = @Translation("Date and time"),
 *   field_types = {
 *     "cbr_datetime"
 *   }
 * )
 */
class CBRDateTimeDefaultWidget extends DateTimeDefaultWidget
{
}