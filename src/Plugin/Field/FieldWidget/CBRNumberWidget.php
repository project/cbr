<?php

namespace Drupal\cbr\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\NumberWidget;

/**
 * Plugin implementation of the 'cbr number' widget.
 *
 * @FieldWidget(
 *   id = "cbr_number",
 *   label = @Translation("Number field"),
 *   field_types = {
 *     "cbr_integer",
 *     "cbr_decimal"
 *   }
 * )
 */
class CBRNumberWidget extends NumberWidget
{
}