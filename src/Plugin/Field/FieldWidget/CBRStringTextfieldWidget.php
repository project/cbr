<?php

namespace Drupal\cbr\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;

/**
 * Plugin implementation of the 'string_textfield' widget.
 *
 * @FieldWidget(
 *   id = "cbr_string_textfield",
 *   label = @Translation("Textfield"),
 *   field_types = {
 *     "cbr_string"
 *   }
 * )
 */
class  CBRStringTextfieldWidget extends StringTextfieldWidget
{
}