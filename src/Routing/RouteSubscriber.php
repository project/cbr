<?

namespace Drupal\cbr\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase
{

    /**
     * {@inheritdoc}
     */
    protected function alterRoutes(RouteCollection $collection)
    {
        if ($route = $collection->get('view.similar_cases.page_1')) {
            $route->setRequirement('_access_similar_cases_tab', 'TRUE');
        }
    }
}